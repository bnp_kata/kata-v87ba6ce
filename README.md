### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### Project Description 
MaxSumOfSubArray project is used to find the maximum sum of the sub array for the given Integer array

### Steps to Test and Run the Project

1) clone the project from https://gitlab.com/bnp_kata/kata-v87ba6ce.git
2) once cloned to local repo , use any Java IDE and import the project as existing maven project
3) Right click on the project and RunAs-> maven build -> Goals enter clean install
4) once project build successfully , Right click on the project -> Run As -> Java Application (Select Application.class as runner)
5) once application is up , goto http://localhost:8092/bnpfortis/subArrayMaxSum and enter the array elements as comma separated value
   Note : make sure inputs are only Integers you are not entering any special characters,empty spaces and extra commas

6)To calculate the sum via Rest API : http://localhost:8092/bnpfortis/subArrayMaxSum
  sample input  
   {
	"array": "1,2,3,-2"
	}