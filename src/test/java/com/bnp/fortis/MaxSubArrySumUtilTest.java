package com.bnp.fortis;

import static org.junit.Assert.*;

import org.junit.Test;

import com.bnp.fortis.model.MaxSubArraySumModel;
import com.bnp.fortis.util.MaxSubArraySumUtil;

public class MaxSubArrySumUtilTest {

	/**
	 * If we pass the empty array list then expected result is 0
	 */
	@Test
	public void testEmptyMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("");
		int expected =0;
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the array with all positive numbers then expected result is sum of the elements present in the array
	 */
	@Test
	public void testPositiveMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("1,2,3");
		int expected =6;
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the array with all negative numbers then expected result is 0
	 */
	@Test
	public void testNegativeMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("-1,-2,-3");
		int expected =0;
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the array with single negative number then expected result is 0
	 */
	@Test
	public void testSingleNegativeNumMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("-1");
		int expected =0;
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the array with single positive number then expected result is that positive number
	 */
	@Test
	public void testSinglePositiveNumMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("2");
		int expected =2;
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the array with mixed elements negative and positive number then expected result is Maximum sum of the sub array
	 */
	
	@Test
	public void testMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("-2,1,-3,4,-1,2,1,-5,4");
		int expected =6;
		
		int actual = MaxSubArraySumUtil.maximumSubarraySum(a);
		System.out.println(actual);
		assertEquals(expected,actual);
	}
	
	/**
	 * If we pass the non Integer array  with greater bytes it should throw NumberFormat Exception
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testNonIntegerMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("2,3.5,2");
		MaxSubArraySumUtil.maximumSubarraySum(a);
	}
	/**
	 * If we pass comma in the prefix then it should throw NumberFormat Exception
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testPreCommaMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray(",1,3,5");
		MaxSubArraySumUtil.maximumSubarraySum(a);
	}
	
	/**
	 * If we pass extra comma in the middle or anywhere in the array then it should throw NumberFormat Exception
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testExtraCommaMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("1,3,,5");
		MaxSubArraySumUtil.maximumSubarraySum(a);
	}

	/**
	 * If we pass any special chars in the array then it should throw NumberFormat Exception
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testSpecialCharMaximumSubarraySum() {
		MaxSubArraySumModel a =new MaxSubArraySumModel();
		a.setArray("1,3,5,&");
		MaxSubArraySumUtil.maximumSubarraySum(a);
	}
	
}
