package com.bnp.fortis.util;
/**
 * @author Vishnu
 * This method is used to calculate the Max sum sub array for given Array 
 */

import com.bnp.fortis.model.MaxSubArraySumModel;

public class MaxSubArraySumUtil {
	
	public static int maximumSubarraySum(MaxSubArraySumModel a) {
		String[] ar= a.getArray().split(",");
		if(ar.length==1 && "".equals(ar[0])) {
			return 0;
		}
		int[] arr=new int[ar.length];
		for(int i=0;i<ar.length;i++) {
			arr[i]=Integer.parseInt(ar[i]);
		}
		int n = arr.length;
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i <= n - 1; i++) {
            int currSum = 0;
            for (int j = i; j <= n - 1; j++) {
            currSum += arr[j];
            if (currSum > maxSum) {
                maxSum = currSum;
            }
            }
        }
        if(maxSum<0) {
        	return 0;
        }
		return maxSum;
	}

}
