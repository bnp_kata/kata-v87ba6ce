package com.bnp.fortis.restcontroller;
/**
 * @author Vishnu
 * his controller class is used to expose the rest API which will help to calculate the Max sum of sub array
 */

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bnp.fortis.model.MaxSubArraySumModel;
import com.bnp.fortis.util.MaxSubArraySumUtil;

@RestController
public class MaxSubArraySumRestController {
	
	@PostMapping("/subArrayMaxSum")
	public int calculate(@RequestBody MaxSubArraySumModel a) {
		return MaxSubArraySumUtil.maximumSubarraySum(a);
	}
}
