package com.bnp.fortis.advice;
/**
 * 
 * @author Vishnu
 * This class consisits data members which will give meaningful error details to the end user
 */
public class ExceptionResponse {

	private String errorMessage;
	private String requestedURI;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRequestedURI() {
		return requestedURI;
	}

	public void callerURL(final String requestedURI) {
		this.requestedURI = requestedURI;
	}
}
