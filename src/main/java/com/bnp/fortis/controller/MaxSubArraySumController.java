package com.bnp.fortis.controller;

/**
 * @author Vishnu
 * This class will act as controller class to invoke the methods used to claculate the max sub array sum and show the reults in UI
 */

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bnp.fortis.model.MaxSubArraySumModel;
import com.bnp.fortis.util.MaxSubArraySumUtil;

@Controller
public class MaxSubArraySumController {

	
	@GetMapping("/subArrayMaxSum")
	public String index() {
		return "subArrayMaxSum";
	}
	
	@PostMapping("/calculate")
	public String calculate(@ModelAttribute("array") MaxSubArraySumModel a,ModelMap modelMap) {
		
		modelMap.addAttribute("res","Max sum of sub array is "+MaxSubArraySumUtil.maximumSubarraySum(a));
		return "subArrayMaxSum";
	}
	
	@ExceptionHandler(NumberFormatException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String handleNumberFormatException(Exception e) {
		
		return "subArrayMaxSumErr";
	}
}
