package com.bnp.fortis.model;

/**
 * 
 * @author Vishnu
 * This class is used to store the user input for MaxSubArraySum
 */

public class MaxSubArraySumModel {
	
	private String array;

	public String getArray() {
		return array;
	}

	public void setArray(String array) {
		this.array = array;
	}
	
	
}
