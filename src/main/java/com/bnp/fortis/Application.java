package com.bnp.fortis;
/**
 * @author Vishnu
 * This calss will start the springBoot application
 */
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	}

